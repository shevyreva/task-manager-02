package ru.t1.shevyreva.tm;

import static ru.t1.shevyreva.tm.constant.TerminalConst.*;

public class Application {

    public static void main(final String[] args) {
        processArguments(args);
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ABOUT:
                showAbout();
            break;
            case VERSION:
                showVersion();
            break;
            case HELP:
                showHelp();
            break;
            default:
                showError();
            break;
        }
    }

    public static void showError() {
        System.err.println("[ERROR]");
        System.err.printf("This argument is not supported. Enter \"%s\" for command list. \n", HELP);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Shevyreva Liya");
        System.out.println("e-mail: liyavmax@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.printf("%s.%s.%s \n", APP_MAJOR_VERSION, APP_MINOR_VERSION, APP_FIXES_VERSION);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show about program. \n", ABOUT);
        System.out.printf("%s - Show program version. \n", VERSION);
        System.out.printf("%s - Show command list. \n", HELP);
    }

}
