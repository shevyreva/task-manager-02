package ru.t1.shevyreva.tm.constant;

public final class TerminalConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final int APP_MAJOR_VERSION = 1;

    public static final int APP_MINOR_VERSION = 2;

    public static final int APP_FIXES_VERSION = 0;

}
